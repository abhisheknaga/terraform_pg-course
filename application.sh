terraform -chdir=$CI_PROJECT_DIR/ init || exit_code=$? 
terraform output | grep  "gitlab" | cut -d "=" -f2 | cut -d '"' -f2 | tail -n1 > ips
cat ips
git clone https://gitlab.com/abhisheknaga/Easy_School.git
cd Easy_School/
ec2metadata --public-ipv4 > ip.txt
cat ip.txt
export IP=$(cat ip.txt | awk -F '=' '{print $1}' | tr -d '"')
sed -i 's/abhi/'$IP'/g' easyschool/settings.py
docker-compose up -d
