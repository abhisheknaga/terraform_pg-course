output "Jenkins" {
  value = zipmap(aws_instance.Jenkins[*].private_ip,aws_instance.Jenkins[*].tags["Name"])
}
output "gitlab" {
  value = zipmap(aws_instance.Gitlab[*].private_ip,aws_instance.Gitlab[*].tags["Name"])
}
output "gitlabpub" {
  value = zipmap(aws_instance.Gitlab[*].public_ip,aws_instance.Gitlab[*].tags["Name"])
}











